<html>
<head>
	<title>Database</title>
</head>
<body>
<?php
$hostname = "localhost";
$username = "root";
$password = "1234";
$db = "classicmodels";

$dbconnect=mysqli_connect($hostname,$username,$password,$db);

if ($dbconnect->connect_error) {
  die("Database connection failed: " . $dbconnect->connect_error);
}

?>
<table border="1" align="center">
<tr>
  <td>id</td>
  <td>name</td>
</tr>

<?php

$query = mysqli_query($dbconnect, "SELECT * FROM Book")
   or die (mysqli_error($dbconnect));

while ($row = mysqli_fetch_array($query)) {  
  echo
    "<tr>
     <td>{$row['id']}</td>
     <td>{$row['name']}</td>
    </tr>";

}
?>
</table>
</body>
</html>
